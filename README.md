# Course_compiler
1、组员分工:
   13307130475 徐佳玮：miniJava.g4语法，生成抽象语法树LISP及GUI显示、词法错误、作用域
   13307130491 周佳音：语法错误、语义错误、实验报告、作用域处理
2、运行代码的方式:
   src中会给出完整的包
3、运行平台:
   windows10 ,Eclipse luna 4.4.2, Antlr4.6
4、说明:
   前期的commit的 Git Log(输出AST)是在Github（https://github.com/icestrawberryxjw/Course_compiler）上的，后来转入到Bitbucket