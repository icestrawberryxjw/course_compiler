grammar miniJava;

Identifier : [a-zA-Z0-9_]+;
INTEGER_LITERAL : INT_HEX | INT_BIN | INT_DEC ;
INT_HEX : '0x'[0-9a-fA-F]+ ;
INT_BIN : '0b'[01]+;
INT_DEC : [0-9]+ ;
WS : [ \r\n\t]+ ->skip;
COMMENT : '//'~[\r\n]* -> skip;

goal : mainClass ( classDeclaration )* EOF;
mainClass : 'class' Identifier '{' 'public' 'static' 'void' 'main' '(' 'String' '[' ']' 
		Identifier ')' '{' statement '}' '}';
	
classDeclaration :  'class' Identifier ('extends' Identifier)? '{' (varDeclaration)*
	( methodDeclaration )* '}' ;

varDeclaration : type Identifier ';' ;

methodDeclaration : 'public' type Identifier '(' formalList ')'
	'{' (varDeclaration)* (statement)* 'return' expression ';' '}' ;
	
formalList: type Identifier (formalRest)*
;

formalRest: ',' type Identifier
;

type : 'int' '[' ']'
	| 'boolean'
	| 'int'
	| Identifier ;
	

statement : '{' (statement)* '}'                         #nestedStmt
	| 'if' '(' expression ')' ifStatement 'else' elseStatement #ifelseStmt
	| 'while' '(' expression ')' statement               #whileStmt
	| 'System.out.println' '(' expression ')'';'         #systemcallStmt
	| Identifier '=' expression ';'                      #variableAssignStmt
	| Identifier '[' expression ']' '=' expression ';'   #arrayAssignStmt
	;

ifStatement
:	statement
;

elseStatement
:	statement
;

expression : expression ( '&&' | '<' | '+' | '-' | '*' ) expression #binaryOp
    | expression '[' expression ']' #getArrExpr
    | expression '.' 'length' #getLengthExpr
    | expression '.' Identifier '(' ( expression ( ',' expression )* )? ')' #getMethodExpr
    | INTEGER_LITERAL     #intliteral
	| 'true'  #literal
	| 'false' #literal
	| Identifier #identifier
	| 'this' #this
	| 'new' 'int' '[' expression ']'  #newArrExpr
	| 'new' Identifier '(' ')' #newExpr
	| ('!' | '~' | '+' | '-' ) expression #unaryOp
	| '(' expression ')'  #parensExpr
	;



