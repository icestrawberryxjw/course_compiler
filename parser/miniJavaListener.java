// Generated from miniJava.g4 by ANTLR 4.6
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link miniJavaParser}.
 */
public interface miniJavaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#goal}.
	 * @param ctx the parse tree
	 */
	void enterGoal(miniJavaParser.GoalContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#goal}.
	 * @param ctx the parse tree
	 */
	void exitGoal(miniJavaParser.GoalContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#mainClass}.
	 * @param ctx the parse tree
	 */
	void enterMainClass(miniJavaParser.MainClassContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#mainClass}.
	 * @param ctx the parse tree
	 */
	void exitMainClass(miniJavaParser.MainClassContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterClassDeclaration(miniJavaParser.ClassDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#classDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitClassDeclaration(miniJavaParser.ClassDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#varDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterVarDeclaration(miniJavaParser.VarDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#varDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitVarDeclaration(miniJavaParser.VarDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void enterMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#methodDeclaration}.
	 * @param ctx the parse tree
	 */
	void exitMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#formalList}.
	 * @param ctx the parse tree
	 */
	void enterFormalList(miniJavaParser.FormalListContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#formalList}.
	 * @param ctx the parse tree
	 */
	void exitFormalList(miniJavaParser.FormalListContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#formalRest}.
	 * @param ctx the parse tree
	 */
	void enterFormalRest(miniJavaParser.FormalRestContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#formalRest}.
	 * @param ctx the parse tree
	 */
	void exitFormalRest(miniJavaParser.FormalRestContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#type}.
	 * @param ctx the parse tree
	 */
	void enterType(miniJavaParser.TypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#type}.
	 * @param ctx the parse tree
	 */
	void exitType(miniJavaParser.TypeContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nestedStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterNestedStmt(miniJavaParser.NestedStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nestedStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitNestedStmt(miniJavaParser.NestedStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ifelseStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterIfelseStmt(miniJavaParser.IfelseStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ifelseStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitIfelseStmt(miniJavaParser.IfelseStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code whileStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterWhileStmt(miniJavaParser.WhileStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code whileStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitWhileStmt(miniJavaParser.WhileStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code systemcallStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterSystemcallStmt(miniJavaParser.SystemcallStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code systemcallStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitSystemcallStmt(miniJavaParser.SystemcallStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code variableAssignStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code variableAssignStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAssignStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterArrayAssignStmt(miniJavaParser.ArrayAssignStmtContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAssignStmt}
	 * labeled alternative in {@link miniJavaParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitArrayAssignStmt(miniJavaParser.ArrayAssignStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void enterIfStatement(miniJavaParser.IfStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#ifStatement}.
	 * @param ctx the parse tree
	 */
	void exitIfStatement(miniJavaParser.IfStatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link miniJavaParser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void enterElseStatement(miniJavaParser.ElseStatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link miniJavaParser#elseStatement}.
	 * @param ctx the parse tree
	 */
	void exitElseStatement(miniJavaParser.ElseStatementContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getLengthExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetLengthExpr(miniJavaParser.GetLengthExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getLengthExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetLengthExpr(miniJavaParser.GetLengthExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewExpr(miniJavaParser.NewExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewExpr(miniJavaParser.NewExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parensExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterParensExpr(miniJavaParser.ParensExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parensExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitParensExpr(miniJavaParser.ParensExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryOp}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryOp(miniJavaParser.UnaryOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryOp}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryOp(miniJavaParser.UnaryOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code newArrExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterNewArrExpr(miniJavaParser.NewArrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code newArrExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitNewArrExpr(miniJavaParser.NewArrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getMethodExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetMethodExpr(miniJavaParser.GetMethodExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getMethodExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetMethodExpr(miniJavaParser.GetMethodExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code binaryOp}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterBinaryOp(miniJavaParser.BinaryOpContext ctx);
	/**
	 * Exit a parse tree produced by the {@code binaryOp}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitBinaryOp(miniJavaParser.BinaryOpContext ctx);
	/**
	 * Enter a parse tree produced by the {@code getArrExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterGetArrExpr(miniJavaParser.GetArrExprContext ctx);
	/**
	 * Exit a parse tree produced by the {@code getArrExpr}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitGetArrExpr(miniJavaParser.GetArrExprContext ctx);
	/**
	 * Enter a parse tree produced by the {@code this}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterThis(miniJavaParser.ThisContext ctx);
	/**
	 * Exit a parse tree produced by the {@code this}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitThis(miniJavaParser.ThisContext ctx);
	/**
	 * Enter a parse tree produced by the {@code identifier}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(miniJavaParser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by the {@code identifier}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(miniJavaParser.IdentifierContext ctx);
	/**
	 * Enter a parse tree produced by the {@code intliteral}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterIntliteral(miniJavaParser.IntliteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code intliteral}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitIntliteral(miniJavaParser.IntliteralContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literal}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterLiteral(miniJavaParser.LiteralContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literal}
	 * labeled alternative in {@link miniJavaParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitLiteral(miniJavaParser.LiteralContext ctx);
}