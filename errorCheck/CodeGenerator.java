package errorCheck;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import java.util.*;

import org.objectweb.asm.*;
import org.objectweb.asm.commons.GeneratorAdapter;

import parser.miniJavaBaseListener;
import parser.miniJavaParser;

import java.io.*;

public class CodeGenerator extends miniJavaBaseListener implements Opcodes{
    public final org.objectweb.asm.commons.Method INIT(){
        return org.objectweb.asm.commons.Method.getMethod("void <init> ()");
    }
    miniJavaParser parser;
    final Map<String, Klass> klasses;
    ParseTreeProperty<Scope> scopes;
    ParseTreeProperty<Klass> callerTypes;
    Scope currentScope = null;
	ClassWriter cw = null;
	org.objectweb.asm.commons.Method currentMethod;
	GeneratorAdapter methodGenerator;
	FileOutputStream fos;
	Stack<Label> labelStack = new Stack<Label>();
    int argCount;
    public CodeGenerator(final Map<String, Klass> klasses, final ParseTreeProperty<Scope> scopes, final ParseTreeProperty<Klass> callerTypes, final miniJavaParser parser){
        this.klasses=klasses;
        this.scopes=scopes;
        this.callerTypes=callerTypes;
        this.parser=parser;
    }
	
    /*---------------------------------Generates Code for Main Class--------------------------------*/
	@Override public void enterMainClass(miniJavaParser.MainClassContext ctx){
		Klass mainKlass = klasses.get(ctx.Identifier(0).getText());
		String mainKlassName = mainKlass.getScopeName();
		cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(V1_1, ACC_PUBLIC, mainKlassName, null, "java/lang/Object", null);

        methodGenerator = new GeneratorAdapter(ACC_PUBLIC, INIT(), null, null, cw);
        methodGenerator.loadThis();
        methodGenerator.invokeConstructor(Type.getType(Object.class), INIT());
        methodGenerator.returnValue();
        methodGenerator.endMethod();

        methodGenerator = new GeneratorAdapter(ACC_PUBLIC + ACC_STATIC, org.objectweb.asm.commons.Method.getMethod("void main (String[])"), null, null, cw);
	}
	@Override public void exitMainClass(miniJavaParser.MainClassContext ctx){
        methodGenerator.returnValue();
        methodGenerator.endMethod();
		cw.visitEnd();
		Klass mainKlass = klasses.get(ctx.Identifier(0).getText());
		String mainKlassName = mainKlass.getScopeName();
		
		try{
			fos = new FileOutputStream(new File(mainKlassName + ".class"));
			fos.write(cw.toByteArray());
	        fos.close();
		}catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
			System.exit(1);
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.exit(1);
		}
	}

    /*----------------------------------Generates Code for Classes----------------------------------*/
	@Override public void enterClassDeclaration(miniJavaParser.ClassDeclarationContext ctx){
        enterScope(ctx);

		Klass klass = klasses.get(ctx.Identifier(0).getText());
		String klassName = klass.getScopeName();
		String superKlassName = klass.getSuperKlass()!=null ?
			klass.getSuperKlass().getScopeName() :
			"java/lang/Object";
		//System.out.println("super = " + superKlassName);

		cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        cw.visit(V1_1, ACC_PUBLIC, klassName, null, superKlassName, null);

        methodGenerator = new GeneratorAdapter(ACC_PUBLIC, INIT(), null, null, cw);
        methodGenerator.loadThis();
        methodGenerator.invokeConstructor(Type.getObjectType(superKlassName), INIT());
        methodGenerator.returnValue();
        methodGenerator.endMethod();
	}

	@Override public void exitClassDeclaration(miniJavaParser.ClassDeclarationContext ctx){
		cw.visitEnd();
		Klass klass = klasses.get(ctx.Identifier(0).getText());
		String klassName = klass.getScopeName();
		
		try{
			fos = new FileOutputStream(new File(klassName + ".class"));
			fos.write(cw.toByteArray());
	        fos.close();
		}catch(FileNotFoundException fnfe){
			fnfe.printStackTrace();
			System.exit(1);
		}catch(IOException ioe){
			ioe.printStackTrace();
			System.exit(1);
		}
		exitScope();
	}

	@Override public void exitVarDeclaration(miniJavaParser.VarDeclarationContext ctx){
        Symbol var = currentScope.lookup(ctx.Identifier().getText());
        if(var.isField()){
			cw.visitField(
				ACC_PROTECTED,
				var.getName(),
				var.getType().asAsmType().getDescriptor(),
				null,
				null)
			.visitEnd();
        }
	}
    /*----------------------------------Generates Code for Classes----------------------------------*/

	@Override public void enterMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx){
		enterScope(ctx);
		Method methodRepresentation = (Method)currentScope;
        currentMethod = methodRepresentation.asAsmMethod();
        methodGenerator = new GeneratorAdapter(ACC_PUBLIC, currentMethod, null, null, cw);
	}
	@Override public void exitMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx){
		methodGenerator.returnValue();
        methodGenerator.endMethod();
		exitScope();
	}
	@Override public void enterFormalList(miniJavaParser.FormalListContext ctx){
		argCount=0;
	}
	@Override public void enterFormalRest(miniJavaParser.FormalRestContext ctx){
		currentScope.lookup(ctx.Identifier().getText()).setParameterIdentifier(argCount);
		argCount++;
	}


	/*------------------------------Generates Code for Printing Integers----------------------------*/
	@Override public void enterSystemcallStmt(miniJavaParser.SystemcallStmtContext ctx){
        methodGenerator.getStatic(Type.getType(System.class), "out", Type.getType(PrintStream.class));
	}
	@Override public void exitSystemcallStmt(miniJavaParser.SystemcallStmtContext ctx){
        methodGenerator.invokeVirtual(Type.getType(PrintStream.class), org.objectweb.asm.commons.Method.getMethod("void println (int)"));
	}
	/*------------------------------Generates Code for Printing Integers----------------------------*/

	/** --------------Generates code for doing field and local variable assignment.-----------------*/
    @Override public void enterVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx){
        Symbol var = currentScope.lookup(ctx.Identifier().getText());
        if(var.isField()){
            methodGenerator.loadThis();
        }
    }
    @Override public void exitVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx){
        Symbol var = currentScope.lookup(ctx.Identifier().getText());
        Type type = var.getType().asAsmType();
        if(var.isField()){
        	//System.out.println("Variable " + var.getName() + " is a field");
            Type owner = ((Klass)currentScope.getEnclosingScope()).asAsmType();
            //methodGenerator.loadThis();
        	methodGenerator.putField(owner, var.getName(), type);
        }else if(var.isParameter()){
        	methodGenerator.storeArg(var.getParameterListIdentifier());
        }else{
        	methodGenerator.storeLocal(var.getLocalIdentifier(), type);
        }
    }
	@Override public void enterArrayAssignStmt(miniJavaParser.ArrayAssignStmtContext ctx){
        Symbol var = currentScope.lookup(ctx.Identifier().getText());
        Type type = var.getType().asAsmType();
        if(var.isField()){
            Type owner = ((Klass)currentScope.getEnclosingScope()).asAsmType();
            methodGenerator.loadThis();
        	methodGenerator.getField(owner, var.getName(), type);
        }else if(var.isParameter()){
        	methodGenerator.loadArg(var.getParameterListIdentifier());
        }else{
        	methodGenerator.loadLocal(var.getLocalIdentifier(), type);
        }
	}
	@Override public void exitArrayAssignStmt(miniJavaParser.ArrayAssignStmtContext ctx){
		methodGenerator.arrayStore(Type.INT_TYPE);
	}

	/** --------------Generates code for doing field and local variable assignment.-----------------*/

	@Override public void enterIfelseStmt(miniJavaParser.IfelseStmtContext ctx){
		Label enterElse = methodGenerator.newLabel();
		Label exitElse  = methodGenerator.newLabel();
		//methodGenerator.ifZCmp(GeneratorAdapter.EQ, enterElse);
		labelStack.push(exitElse);
		labelStack.push(enterElse);
		labelStack.push(exitElse);
		labelStack.push(enterElse);
	}
	@Override public void enterIfStatement(miniJavaParser.IfStatementContext ctx) {
		methodGenerator.ifZCmp(GeneratorAdapter.EQ, labelStack.pop());
	}
	@Override public void exitIfStatement(miniJavaParser.IfStatementContext ctx){
		methodGenerator.goTo(labelStack.pop());
	}
	@Override public void enterElseStatement(miniJavaParser.ElseStatementContext ctx) {
		methodGenerator.mark(labelStack.pop());
	}
	@Override public void exitElseStatement(miniJavaParser.ElseStatementContext ctx) {
		methodGenerator.mark(labelStack.pop());
	}

	//@Override public void enterArrayAccessExpression(@NotNull MinijavaParser.ArrayAccessExpressionContext ctx) { }
	@Override public void exitGetArrExpr(miniJavaParser.GetArrExprContext ctx){
		methodGenerator.arrayLoad(Type.INT_TYPE);
	}
	//@Override public void enterArrayLengthExpression(@NotNull MinijavaParser.ArrayLengthExpressionContext ctx) { }
	@Override public void exitGetLengthExpr(miniJavaParser.GetLengthExprContext ctx){
		methodGenerator.arrayLength();
	}

	@Override public void exitGetMethodExpr(miniJavaParser.GetMethodExprContext ctx){
		Klass klass = callerTypes.get(ctx);
		methodGenerator.invokeVirtual(klass.asAsmType(), ((Method)klass.lookup(ctx.Identifier().getText() + "()")).asAsmMethod());
	}
	@Override public void enterIntliteral(miniJavaParser.IntliteralContext ctx){
		methodGenerator.push(Integer.parseInt(ctx.getText()));
	}
	@Override public void exitIdentifier(miniJavaParser.IdentifierContext ctx){
		Symbol var = currentScope.lookup(ctx.Identifier().getText());
        Type type = var.getType().asAsmType();
        if(var.isParameter()){
        	methodGenerator.loadArg(var.getParameterListIdentifier());
        }else if(var.isField()){
        	//System.out.println("Variable " + var.getName() + " is a field");
            Type owner = ((Klass)currentScope.getEnclosingScope()).asAsmType();
            methodGenerator.loadThis();
        	methodGenerator.getField(owner, var.getName(), type);
        }else{
        	methodGenerator.loadLocal(var.getLocalIdentifier(), type); 
        }
	}
	@Override public void exitThis(miniJavaParser.ThisContext ctx){
		methodGenerator.loadThis();
	}
	//@Override public void enterArrayInstantiationExpression(@NotNull MinijavaParser.ArrayInstantiationExpressionContext ctx) { }
	@Override public void exitNewArrExpr(miniJavaParser.NewArrExprContext ctx){
		methodGenerator.newArray(Type.INT_TYPE);
	}
    @Override public void enterNewExpr(miniJavaParser.NewExprContext ctx) {
        Type type = Type.getObjectType(ctx.Identifier().getText());
        methodGenerator.newInstance(type);
        methodGenerator.dup();
        methodGenerator.invokeConstructor(type, INIT());
    }
    public void enterScope(ParserRuleContext ctx){
        currentScope = scopes.get(ctx);
    }
    private void exitScope(){
        currentScope = currentScope.getEnclosingScope();
    }
}
