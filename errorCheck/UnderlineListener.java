/***
 * Excerpted from "The Definitive ANTLR 4 Reference",
***/
package errorCheck;

import org.antlr.v4.runtime.misc.Nullable;
import org.antlr.v4.runtime.*;
import java.util.*;

/**
 * An error listener for printing information about syntax errors
 * This ErrorListener should be added to a parser.  
 * Whenever a syntax error occurs, the syntaxError() method is called.
 * The follows follows set for the previous token is printed and
 * the offending token is underlined. 
 */
public class UnderlineListener extends BaseErrorListener {
	@Override public void syntaxError(Recognizer<?,?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e){
        ErrorPrinter.printFileNameAndLineNumber((Token)offendingSymbol);
        System.err.println("line " + line + ":" + charPositionInLine + " " + msg);
        ErrorPrinter.underlineError(recognizer,(Token)offendingSymbol);
}
