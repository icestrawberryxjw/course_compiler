package errorCheck;

import java.util.*;
import org.objectweb.asm.*;

/**
 * A symbol-table representation of a type. 
 */
public class Klass implements Scope{
	private Klass superKlass;

    //The name of this Klass.
	private String name;

	private Map<String, Symbol> symTable = new HashMap<String, Symbol>();

    public Klass(String name){
       this.name = name; 
    }

    public void setSuperKlass(Klass superKlass){
        this.superKlass = superKlass;
    }
 
    public Klass getSuperKlass(){
    	return this.superKlass;
    }

    @Override public String getScopeName(){
    	return name;
    }
    
    @Override public Scope getEnclosingScope(){return null;}

    @Override public void define(Symbol sym){
    	symTable.put(sym.getName(), sym);
    }

    @Override public void initialize(Symbol sym){
        assert false;
    }

    public boolean isDescendantOf(Klass other){
        if(this.superKlass==null&&other!=this){
            return false;
        }else if(other==this){
            return true;
        }else{
            return this.superKlass.isDescendantOf(other);
        }
    }

    @Override public Symbol lookup(String name){
    	Symbol symbol = null;
    	for(Klass klass = this; symbol==null&&klass!=null; klass=klass.getSuperKlass()){
    		symbol = klass.symTable.get(name);
    	}
    	return symbol;
    }

    /**
     * Searches this klass for a symbol with the given name.
     * If no such symbol exists, return null.
     * @param name The name of the symbol to search for.
     * @return the symbol with the given name, or null if no such symbol exists.
     */
    @Override public Symbol lookupLocally(String name){
    	return symTable.get(name);
    }
   
    @Override public boolean hasBeenInitialized(String name){
        return this.lookup(name)!=null;
    }
    
    @Override public Set<Symbol> getInitializedVariables(){
        assert false;
        return null;
    }

    /**
     * @return The name of this klass
     */
    public String toString(){
    	return name;
    }

    /**
     * @return an asm Type representation of this class.
     */
    public Type asAsmType(){
        if(this.name.equals("int")){
            return Type.INT_TYPE;
        }else if(this.name.equals("boolean")){
            return Type.BOOLEAN_TYPE;
        }else if(this.name.equals("int[]")){
            return Type.getType(int[].class);
        }else{
            return Type.getType("L" + this.name + ";");
        }
    }
}