package errorCheck;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import parser.miniJavaBaseListener;
import parser.miniJavaParser;

import java.util.*;

/**
 * A listener mechanism for creating and naming the klasses in the symbol table.
 */
public class ClassNamer extends miniJavaBaseListener {

    private Map<String, Klass> klasses;

    private miniJavaParser parser;

    public ClassNamer(Map<String, Klass> klasses, miniJavaParser parser){
        this.klasses = klasses;
        this.parser  = parser;
    }
    /**
     * Creates a klass in the symbol-table with the name derived from the
     * context.  Prints a duplicateClassError if the symbol-table already 
     * contains a klass with that name.
     */
    @Override public void enterClassDeclaration(@NotNull miniJavaParser.ClassDeclarationContext ctx) { 
        Klass currentKlass = new Klass(ctx.Identifier(0).getText());
        if(klasses.put(currentKlass.getScopeName(), currentKlass)!=null){
            ErrorPrinter.printDuplicateClassError(parser, ctx.Identifier(0).getSymbol(), currentKlass.getScopeName());
        }
    }
    /**
     * Adds the main class along with the primitive types to the symbol-table
     */
    @Override public void enterMainClass(@NotNull miniJavaParser.MainClassContext ctx) {
        Klass currentKlass;
        currentKlass = new Klass("int[]");
        klasses.put(currentKlass.getScopeName(),currentKlass);
        currentKlass = new Klass("int");
        klasses.put(currentKlass.getScopeName(),currentKlass);
        
        currentKlass = new Klass("boolean");
        klasses.put(currentKlass.getScopeName(),currentKlass);
        
        currentKlass = new Klass(ctx.Identifier(0).getText());
        klasses.put(currentKlass.getScopeName(), currentKlass);
    }
}
