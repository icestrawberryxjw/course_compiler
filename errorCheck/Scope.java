package errorCheck;

import java.util.*;

/** 
 * An interface for defining scopes and scoping rules for miniJava. 
 */
public interface Scope {
    public String getScopeName();
	
    public Scope getEnclosingScope();

    public void define(Symbol sym);


    public void initialize(Symbol sym);

    public Symbol lookup(String name);
    
    public Symbol lookupLocally(String name);
    
c   boolean hasBeenInitialized(String name);

    public Set<Symbol> getInitializedVariables();

	public static Klass getEnclosingKlass(Scope scope){
		while(!(scope instanceof Klass)){
			scope=scope.getEnclosingScope();
		}
		return (Klass)scope;
	}

    /**
     * @param  scope A scope for which you want to know the Method that contains it.
     * @return       the Method that contains scope.
     */
    public static Method getEnclosingMethod(Scope scope){
        while(!(scope instanceof Method)){
            scope = scope.getEnclosingScope();
        }
        return (Method)scope;
    }
}
