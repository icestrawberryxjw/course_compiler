package errorCheck;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.ParseTree;

import parser.miniJavaBaseVisitor;
import parser.miniJavaParser;

import java.util.*;

/**
 * A visitor mechanism for performing static type-checking.
 */
public class TypeChecker extends miniJavaBaseVisitor<Klass> {
	private final Map<String, Klass> klasses;
	private final ParseTreeProperty<Scope> scopes;
	private Scope currentScope;
	private ParseTreeProperty<Klass> callerTypes;
	private final miniJavaParser parser;
	private final Klass INT;
	private final Klass INTARRAY;
	private final Klass BOOLEAN;

	public TypeChecker(final Map<String, Klass> klasses, ParseTreeProperty<Scope> scopes, ParseTreeProperty<Klass> callerTypes, miniJavaParser parser){
		INT = klasses.get("int");
		this.klasses = klasses;
		this.scopes=scopes;
		this.callerTypes = callerTypes;        
		//INT = klasses.get("int");
		INTARRAY = klasses.get("int[]");
		BOOLEAN = klasses.get("boolean");
		this.parser=parser;
	}
	@Override public Klass visitMainClass(miniJavaParser.MainClassContext ctx){
		return scopingCall(ctx);
	} 
	@Override public Klass visitClassDeclaration(miniJavaParser.ClassDeclarationContext ctx) { 
		return scopingCall(ctx); 
	}
	@Override public Klass visitMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx) { 
		currentScope = scopes.get(ctx);

		Klass originalKlass = ((Klass)(currentScope.getEnclosingScope())).getSuperKlass();
		Method originalMethod;
		if(originalKlass==null){ originalMethod=null; }
		else{ originalMethod=(Method)originalKlass.lookup(currentScope.getScopeName());}
			
		Method currentMethod = (Method)currentScope;
		Klass currentKlass = (Klass)currentMethod.getEnclosingScope();
        if(originalMethod!=null && originalMethod.getType() != currentMethod.getType()){
            ErrorPrinter.printIncompatibleReturnTypeError(parser, ctx.Identifier().getSymbol(), originalKlass, currentKlass, originalMethod, currentMethod);
        }

		Klass result =  visitChildren(ctx);
		currentScope = currentScope.getEnclosingScope();
        return null;
	}
	@Override public Klass visitType(@NotNull miniJavaParser.TypeContext ctx) {
        //Correctly reported error during variable intialization test with non-existent class.
		if(ctx.Identifier()!=null){//it is a reference type
	        String name = ctx.Identifier().getSymbol().getText();
	        Klass var = klasses.get(name);
	        if(var==null){
	        	ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "class", Scope.getEnclosingKlass(currentScope));
	        }
	        return var;
	    }
	    return null;
	}
	@Override public Klass visitIfelseStmt(miniJavaParser.IfelseStmtContext ctx) {
        //Correctly reported error with int instead of boolean.
		Klass booleanExpression = visit(ctx.expression());
		visit(ctx.ifStatement());
		visit(ctx.elseStatement());
		if(booleanExpression!=BOOLEAN){
		ErrorPrinter.printRequiredFoundError(
			"error: incompatible types.", parser, '(', BOOLEAN.toString(), booleanExpression.toString());
		}
		return null;
	}
	@Override public Klass visitSystemcallStmt(miniJavaParser.SystemcallStmtContext ctx) {
        //Correctly reported error of boolean instead of int.
		Klass printContents = visit(ctx.expression());
		if(printContents!=null && printContents!=INT){
		ErrorPrinter.printRequiredFoundError(
			"error: incompatible types.", parser, '(', INT.toString(), printContents.toString());
		}
		return null;
	}
    @Override public Klass visitVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx){
        //correctly reported errors in all cases.
        String name = ctx.Identifier().getSymbol().getText();
        Symbol var = currentScope.lookup(name);

        Klass rightSide = visit(ctx.expression());
        if ( var==null ) {
        	ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "variable", Scope.getEnclosingKlass(currentScope));
        }else if(rightSide!=null && !rightSide.isDescendantOf(var.getType())){
	        	ErrorPrinter.printRequiredFoundError1("error: incompatible types.", parser, ctx.Identifier().getSymbol(), var.getType().toString(), (rightSide.toString()));
    	}
       	return null;
    }
	@Override public Klass visitArrayAssignStmt(miniJavaParser.ArrayAssignStmtContext ctx) {
        //correctly reported errors in all cases
		//ErrorPrinter.printFileNameAndLineNumber(ctx.Identifier().getSymbol());
        String name = ctx.Identifier().getSymbol().getText();
        Symbol var = currentScope.lookup(name);
        Klass index = visit(ctx.expression(0));
        Klass rightSide = visit(ctx.expression(1));
        if ( var==null ) {
        	ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "variable", Scope.getEnclosingKlass(currentScope));
        }else if(var.getType()!=INTARRAY){
	       	ErrorPrinter.printRequiredFoundError("error: incompatible types.", parser, '[', INTARRAY.toString(), (var.getType().toString()));
        }else if(rightSide!=null && INT!=rightSide){
	       	ErrorPrinter.printRequiredFoundError("error: incompatible types.", parser, '[', INT.toString(), (rightSide.toString()));
        }else if(index!=INT){
        	ErrorPrinter.printRequiredFoundError("error: incompatible types.", parser, '[', INT.toString(), index.toString());
        }
        return null;
	}

	@Override public Klass visitGetArrExpr(miniJavaParser.GetArrExprContext ctx) {
        //Error reporting test successful.
		Klass array = visit(ctx.expression(0));
		Klass index = visit(ctx.expression(1));
		if(array!=INTARRAY){
			//ErrorPrinter.printFileNameAndLineNumber(ctx.LSB().getSymbol());
			System.err.println("error: array required, but " + array + " found");
			//ErrorPrinter.underlineError(parser, ctx.LSB().getSymbol());
		}
		if(index!=INT){
			ErrorPrinter.printRequiredFoundError("error: incompatible type.", parser, '[', INT.toString(), index.toString());
		}
		return INT;
	}
	@Override public Klass visitGetMethodExpr(miniJavaParser.GetMethodExprContext ctx) {
		Klass type = visit(ctx.expression(0));
		callerTypes.put(ctx, type);
		if(type==null){
			return null;
		}
		String methodName = ctx.Identifier().getText() +"()";
		Method method = (Method)(type.lookup(methodName));
        if (method==null ) {
        		ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "method", type);
        		return null;
    	}else{
			List<Klass> parameterList = new ArrayList<Klass>();
			for(miniJavaParser.ExpressionContext expCtx : ctx.expression().subList(1, ctx.expression().size())){
				parameterList.add(visit(expCtx));
			}
			List<Klass> parameterListDefinition = method.getParameterListDefinition();
            if(parameterListDefinition.size()!=parameterList.size()){
				ErrorPrinter.printRequiredFoundError1(
					"error: method call parameters of method " + method.getName() + " do not match method definition.",
					parser, ctx.Identifier().getSymbol(), parameterListDefinition.toString(), parameterList.toString());
                System.err.println("reason: actual and formal argument lists differ in length.");
                return method.getType();
            }
            for(int i=0; i<parameterListDefinition.size(); i++){
                if(!parameterList.get(i).isDescendantOf(parameterListDefinition.get(i))){
				ErrorPrinter.printRequiredFoundError1(
					"error: method call parameters of method " + method.getName() + " do not match method definition.",
					parser, ctx.Identifier().getSymbol(), parameterListDefinition.toString(), parameterList.toString());
                }
            }
			return method.getType();
		}
	}
	@Override public Klass visitIdentifier(miniJavaParser.IdentifierContext ctx) {
		String name = ctx.Identifier().getSymbol().getText();
        Symbol var = currentScope.lookup(name);
        if ( var==null ){
        	ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "variable", Scope.getEnclosingKlass(currentScope));
        	return null;
        }
		return var.getType();
	}
	@Override public Klass visitThis(miniJavaParser.ThisContext ctx) { 
		visitChildren(ctx); 
		return Scope.getEnclosingKlass(currentScope);
	}
	
	@Override public Klass visitNewArrExpr(miniJavaParser.NewArrExprContext ctx) {
		Klass type = visit(ctx.expression());
		if(type!=INT){
			ErrorPrinter.printRequiredFoundError("error: incompatible types.", parser, '[', INT.toString(), type.toString());
		}
		return INTARRAY;
	}
	@Override public Klass visitNewExpr(miniJavaParser.NewExprContext ctx) {
		Klass type = klasses.get(ctx.Identifier().getText());
        if ( type==null ){
        	ErrorPrinter.printUnresolvedSymbolError(parser, ctx.Identifier().getSymbol(), "class", Scope.getEnclosingKlass(currentScope));
        }
		return type;
	}

	@Override public Klass visitParensExpr(miniJavaParser.ParensExprContext ctx) {
		return visit(ctx.expression());
	}
	public Klass scopingCall(ParserRuleContext ctx){
		currentScope = scopes.get(ctx);
		Klass result =  visitChildren(ctx);
		currentScope = currentScope.getEnclosingScope();
		return null;
	}
}
