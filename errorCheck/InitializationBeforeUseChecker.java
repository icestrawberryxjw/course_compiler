package errorCheck;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import parser.miniJavaBaseVisitor;
import parser.miniJavaParser;

import java.util.*;

public class InitializationBeforeUseChecker extends miniJavaBaseVisitor<Set<Symbol>> {
    miniJavaParser parser;
    final Map<String, Klass> klasses;
    ParseTreeProperty<Scope> scopes;
    Scope currentScope = null;

    public InitializationBeforeUseChecker(final Map<String, Klass> klasses, ParseTreeProperty<Scope> scopes, miniJavaParser parser){
        this.scopes=scopes;
        this.klasses=klasses;
        this.parser=parser;
    }
    @Override public Set<Symbol> visitClassDeclaration(miniJavaParser.ClassDeclarationContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null; 
    }

    @Override public Set<Symbol> visitMethodDeclaration(miniJavaParser.MethodDeclarationContext ctx) {
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null; 
    }
    //@Override public void enterIfElseStatement(@NotNull MinijavaParser.IfElseStatementContext ctx) {}
    @Override public Set<Symbol> visitIfelseStmt(miniJavaParser.IfelseStmtContext ctx) {
        visit(ctx.expression());
        Set<Symbol> initializedVariables = visit(ctx.ifStatement());
        initializedVariables.retainAll(visit(ctx.elseStatement()));
        for(Symbol sym : initializedVariables){
            currentScope.initialize(sym);
        }
        return initializedVariables;
    }
    @Override public Set<Symbol> visitIfStatement(miniJavaParser.IfStatementContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        Set<Symbol> ifInit = currentScope.getInitializedVariables();
        exitScope();
        return ifInit;
    }
    @Override public Set<Symbol> visitElseStatement(miniJavaParser.ElseStatementContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        Set<Symbol> elseInit = currentScope.getInitializedVariables();
        exitScope();
        return elseInit;
    }
    @Override public Set<Symbol> visitWhileStmt(miniJavaParser.WhileStmtContext ctx) { 
        enterScope(ctx);
        visitChildren(ctx);
        exitScope();
        return null;
    }
    @Override public Set<Symbol> visitVariableAssignStmt(miniJavaParser.VariableAssignStmtContext ctx) {
        Set<Symbol> sym = visitChildren(ctx);
        currentScope.initialize(currentScope.lookup(ctx.Identifier().getText()));
        return sym;
    }
    @Override public Set<Symbol> visitIdentifier(miniJavaParser.IdentifierContext ctx) {
        String identifier = ctx.Identifier().getText();
        if(!currentScope.hasBeenInitialized(identifier)){
            ErrorPrinter.printVariableMayNotHaveBeenInitializedError(parser, ctx.Identifier().getSymbol(), identifier);
        }
        return visitChildren(ctx);
    }
    public void enterScope(ParserRuleContext ctx){
        currentScope = scopes.get(ctx);
    }
    private void exitScope(){
        currentScope = currentScope.getEnclosingScope();
    }
}
