package errorCheck;

//import org.antlr.v4.runtime.misc.Nullable;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.ParseTreeProperty;

import parser.miniJavaLexer;
import parser.miniJavaParser;

import java.io.*;
import java.util.*;

public class main{
	private static String inputFile = null;

    /**
     * Compiles miniJava programs, reporting syntax and semantic errors.
     * @throws IOException           If an IO error occurs while parsing the file.
     * @throws FileNotFoundException If the specified file does not exist.
     */
	public static void main() throws IOException, FileNotFoundException {
        //The file input stream for lexing the file.  
		InputStream is = new FileInputStream("./src/testError/testError.Java"); 
        ANTLRInputStream input = new ANTLRInputStream(is);
        miniJavaLexer lexer = new miniJavaLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        miniJavaParser parser = new miniJavaParser(tokens);
        Map<String, Klass> klasses = new HashMap<String, Klass>();
        ParseTreeProperty<Scope> scopes = new ParseTreeProperty<Scope>();
        ParseTreeProperty<Klass> callerTypes = new ParseTreeProperty<Klass>();
		parser.removeErrorListeners(); 
        //Reports ambiguities or errors in the grammar
        parser.addErrorListener(new DiagnosticErrorListener());
		parser.getInterpreter()
		.setPredictionMode(PredictionMode.LL_EXACT_AMBIG_DETECTION);
        //Reports syntax errors upon construction of the parse tree.
		parser.addErrorListener(new UnderlineListener());
        //Construct the parse tree, reporting all syntax errors as stated above.
		ParseTree tree = parser.goal();
        ErrorPrinter.exitOnErrors();
        ClassNamer namer = new ClassNamer(klasses, parser); 
        ParseTreeWalker.DEFAULT.walk(namer, tree);
        ErrorPrinter.exitOnErrors();        
        //building the symbol table
        AssignmentListener assigner = new AssignmentListener(klasses, scopes, parser);
        ParseTreeWalker.DEFAULT.walk(assigner, tree); 
        ErrorPrinter.exitOnErrors();        
        //type checking
        TypeChecker typeChecker = new TypeChecker(klasses, scopes, callerTypes, parser);
        // type check errors
        typeChecker.visit(tree);
        ErrorPrinter.exitOnErrors();        
        InitializationBeforeUseChecker iBeforeUChecker = new InitializationBeforeUseChecker(klasses, scopes, parser);
        iBeforeUChecker.visit(tree);
        ErrorPrinter.exitOnErrors();        
        CodeGenerator codeGenerator = new CodeGenerator(klasses, scopes, callerTypes, parser);
        ParseTreeWalker.DEFAULT.walk(codeGenerator, tree);
	}
    public static String getFileName(){
    	return inputFile;
    }
}
